package app.example.android12review.feature.main

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Person
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationManagerCompat
import app.example.android12review.R
import java.util.UUID

@RequiresApi(Build.VERSION_CODES.S)
object CallNotificationsUtil {

    private val incomingCaller = Person.Builder()
        .setName("Jane Doe")
        .setImportant(true)
        .build()

    private val secondCaller = Person.Builder()
        .setName("Jane Doe jr")
        .setImportant(true)
        .build()

    fun Context.onShowNotification(event: CallEvent) {
        val notifyID = UUID.randomUUID().version()
        val channelID = "NOTIFICATIONS_CHANNEL"

        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            Intent(this, FullscreenActivity::class.java),
            PendingIntent.FLAG_MUTABLE
        )

        val callStyle = event.getCallStyle(pendingIntent)

        val notification = Notification.Builder(this, channelID)
            .setContentTitle("Call example notification")
            .setContentText("You received this call")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setStyle(callStyle)
            .addPerson(secondCaller)
            .setFullScreenIntent(pendingIntent, true)
            .build()

        NotificationManagerCompat.from(this).notify(notifyID, notification)
    }

    private fun CallEvent.getCallStyle(pendingIntent: PendingIntent) =
        when (this) {
            is CallEvent.IncomingCall -> Notification.CallStyle.forIncomingCall(
                incomingCaller,
                pendingIntent,
                pendingIntent
            )
            is CallEvent.OngoingCall ->
                Notification.CallStyle.forOngoingCall(incomingCaller, pendingIntent)
            is CallEvent.ScreeningCall -> Notification.CallStyle.forScreeningCall(
                incomingCaller,
                pendingIntent,
                pendingIntent
            )
        }
}