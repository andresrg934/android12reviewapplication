package app.example.android12review.feature.authentication.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import app.example.android12review.feature.authentication.signup.SignUpViewState

class LoginViewModel(
    val presenter: LoginPresenter,
    val viewState: LoginViewState
) : ViewModel() {

    internal class Factory(private val componentsProvider: LoginComponentsProvider) : ViewModelProvider.Factory {
        companion object {
            var newInstance = { componentsProvider: LoginComponentsProvider ->
                Factory(componentsProvider)
            }
        }

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            val viewState = componentsProvider.getLoginViewState()
            val presenter = componentsProvider.getLoginPresenter(viewState)
            return LoginViewModel(presenter, viewState) as? T ?: throw IllegalArgumentException("This factory can only create MainViewModel instances")
        }
    }
}

internal fun getLoginViewModel(
    fragmentScope: LoginFragment
): LoginViewModel {
    val componentsProvider = LoginComponentsProvider()
    return ViewModelProviders
        .of(fragmentScope, LoginViewModel.Factory.newInstance(componentsProvider))
        .get(LoginViewModel::class.java)
}