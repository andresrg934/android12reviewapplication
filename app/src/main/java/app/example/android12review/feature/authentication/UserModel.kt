package app.example.android12review.feature.authentication

data class UserModel(
    val email: String,
    val password: String
)