package app.example.android12review.feature.authentication.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBindingKtx
import androidx.lifecycle.Observer
import app.example.android12review.R
import androidx.databinding.ViewDataBinding
import app.example.android12review.BR
import app.example.android12review.databinding.LoginFragmentBinding
import app.example.android12review.feature.authentication.AuthenticationNavigationEvent
import app.example.android12review.feature.authentication.UserModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlin.coroutines.CoroutineContext

class LoginFragment(
    onLoginRequest: suspend (UserModel) -> Boolean,
    onAuthenticationNavigation: (AuthenticationNavigationEvent) -> Unit,
    override val coroutineContext: CoroutineContext = Dispatchers.IO
) : Fragment(), CoroutineScope {

    private val onLoginObserver = Observer<UserModel> {
        val result = runCatching {
            runBlocking {
                onLoginRequest(it)
            }
        }.getOrThrow()

        if (!result) {
            // TODO do something in case of failure
        }
    }

    private val onNavigationRequest = Observer<AuthenticationNavigationEvent> {
        onAuthenticationNavigation(it)
    }

    private val viewModel by lazy {
        getLoginViewModel(this)
    }

    private val presenter by lazy { viewModel.presenter }
    private val viewState by lazy { viewModel.viewState }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.login_fragment, container, false)

    override fun onViewCreated(safeView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(safeView, savedInstanceState)

        DataBindingUtil.setContentView<LoginFragmentBinding>(requireActivity(), R.layout.login_fragment).apply {
            presenter = presenter
            lifecycleOwner = viewLifecycleOwner
        }

        viewState.loginRequestTrigger.observe(viewLifecycleOwner, onLoginObserver)
        viewState.signupNavigationTrigger.observe(viewLifecycleOwner, onNavigationRequest)
    }

}