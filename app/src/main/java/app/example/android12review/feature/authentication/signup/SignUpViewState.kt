package app.example.android12review.feature.authentication.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.example.android12review.feature.authentication.AuthenticationNavigationEvent
import app.example.android12review.feature.authentication.UserModel

class SignUpViewState {
    private val _signupRequestTrigger = MutableLiveData<UserModel>()
    val signupRequestTrigger: LiveData<UserModel>
        get() = _signupRequestTrigger

    private val _signupNavigation = MutableLiveData<AuthenticationNavigationEvent>()
    val signupNavigation: LiveData<AuthenticationNavigationEvent>
        get() = _signupNavigation

    fun requestSignUp(userModel: UserModel) {
        _signupRequestTrigger.postValue(userModel)
    }

    fun goToSignUp() {
        _signupNavigation.postValue(AuthenticationNavigationEvent.SignUpNavigation)
    }
}