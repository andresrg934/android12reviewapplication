package app.example.android12review.feature.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.fragment.app.Fragment
import app.example.android12review.R
import app.example.android12review.feature.authentication.login.LoginFragment
import app.example.android12review.feature.authentication.signup.SignUpFragment
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

private const val LOGIN_FRAGMENT_TAG = "tag:LoginFragment"
private const val SIGNUP_FRAGMENT_TAG = "tag:SignUpFragment"

@ExperimentalCoroutinesApi
class AuthenticationActivity : AppCompatActivity() {

    private val auth by lazy {
        Firebase.auth
    }

    private val loginFragment =
        supportFragmentManager.findFragmentByTag(LOGIN_FRAGMENT_TAG) as? LoginFragment
            ?: LoginFragment(
                onLoginRequest = ::loginUser,
                onAuthenticationNavigation = ::onAuthenticationNavigation
            )

    private val signUpFragment =
        supportFragmentManager.findFragmentByTag(SIGNUP_FRAGMENT_TAG) as? SignUpFragment
            ?: SignUpFragment(
                onSignUpRequest = ::registerUser,
                onAuthenticationNavigation = ::onAuthenticationNavigation
            )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val splashScreen = installSplashScreen()

        setContentView(R.layout.authentication_activity)
        if (savedInstanceState == null) {
            navigateToFragment(loginFragment)
        }
    }

    private fun setupPreDrawListener() {
        // Set up an OnPreDrawListener to the root view.
        val content: View = findViewById(android.R.id.content)
        content.viewTreeObserver.addOnPreDrawListener(
            object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    // Check if the initial data is ready.
                    val currentUser = auth.currentUser
                    return if (currentUser != null) {
                        // TODO start Main activity before hiding splash screen
                        // The content is ready, we hide splash screen
                        content.viewTreeObserver.removeOnPreDrawListener(this)
                        true
                    } else {
                        // The content is not ready; suspend.
                        false
                    }
                }
            }
        )
    }

    private suspend fun loginUser(userData: UserModel) =
        suspendCancellableCoroutine<Boolean> { continuation ->
            auth.signInWithEmailAndPassword(userData.email, userData.password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, Go to main screen
                        val user = auth.currentUser
                        continuation.resume(true)
                    } else {
                        // If sign in fails, display a message to the user.
                        continuation.resume(false)
                    }
                }
        }

    private suspend fun registerUser(userData: UserModel) =
        suspendCancellableCoroutine<Boolean> { continuation ->
            auth.createUserWithEmailAndPassword(userData.email, userData.password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign up success, return to login
                        val user = auth.currentUser
                        continuation.resume(true)
                    } else {
                        // If sign in fails, display a message to the user.
                        continuation.resume(false)
                    }
                }
        }

    private fun onAuthenticationNavigation(event: AuthenticationNavigationEvent) {
        when (event) {
            is AuthenticationNavigationEvent.LoginNavigation -> {
                navigateToFragment(loginFragment)
            }
            is AuthenticationNavigationEvent.SignUpNavigation -> {
                navigateToFragment(signUpFragment)
            }
        }
    }

    private fun navigateToFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commitNow()
    }
}