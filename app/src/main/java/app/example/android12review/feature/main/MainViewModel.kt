package app.example.android12review.feature.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

class MainViewModel(
    val viewState: MainViewState,
    val presenter: MainPresenter
): ViewModel() {
    internal class Factory : ViewModelProvider.Factory {
        companion object {
            var newInstance = { Factory() }
        }

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            val viewState = MainViewState()
            val presenter = MainPresenter(viewState)
            return MainViewModel(viewState, presenter) as? T ?: throw IllegalArgumentException("This factory can only create MainViewModel instances")
        }
    }
}

internal fun getMainViewModel(
    activityScope: MainReviewActivity
): MainViewModel {
    return ViewModelProviders
        .of(activityScope, MainViewModel.Factory.newInstance())
        .get(MainViewModel::class.java)
}