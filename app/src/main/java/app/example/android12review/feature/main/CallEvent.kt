package app.example.android12review.feature.main

sealed class CallEvent {
    object IncomingCall: CallEvent()
    object OngoingCall: CallEvent()
    object ScreeningCall: CallEvent()
}
