package app.example.android12review.feature.authentication

sealed class AuthenticationNavigationEvent {
    object LoginNavigation: AuthenticationNavigationEvent()
    object SignUpNavigation: AuthenticationNavigationEvent()
}
