package app.example.android12review.feature.authentication.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.example.android12review.feature.authentication.AuthenticationNavigationEvent
import app.example.android12review.feature.authentication.UserModel

class LoginViewState {

    private val _loginRequestTrigger = MutableLiveData<UserModel>()
    val loginRequestTrigger: LiveData<UserModel>
        get() = _loginRequestTrigger

    private val _signupNavigationTrigger = MutableLiveData<AuthenticationNavigationEvent>()
    val signupNavigationTrigger: LiveData<AuthenticationNavigationEvent>
        get() = _signupNavigationTrigger

    fun requestLogin(userModel: UserModel) {
        _loginRequestTrigger.postValue(userModel)
    }

    fun goToSignUp() {
        _signupNavigationTrigger.postValue(AuthenticationNavigationEvent.SignUpNavigation)
    }
}