package app.example.android12review.feature.authentication.login

import app.example.android12review.feature.authentication.UserModel

class LoginPresenter(
    private val viewState: LoginViewState
) {
    fun onLoginRequest(user: String, password: String) {
        if (user.isNotEmpty() && password.isNotEmpty()) {
            val userModel = UserModel(user, password)
            viewState.requestLogin(userModel)
        } else {
            // TODO handle unable to login
        }
    }

    fun onSignUpNavigation() {
        viewState.goToSignUp()
    }
}