package app.example.android12review.feature.authentication.login

class LoginComponentsProvider {

    internal fun getLoginViewState() = LoginViewState()

    internal fun getLoginPresenter(viewState: LoginViewState) = LoginPresenter(viewState)
}