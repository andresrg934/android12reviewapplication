package app.example.android12review.feature.main

class MainPresenter(
    val viewState: MainViewState
) {

    fun onIncomingCallNotificationClicked() {
        viewState.sendIncomingCallEvent()
    }

    fun onOngoingCallNotificationClicked() {
        viewState.sendOngoingCallEvent()
    }

    fun onScreeningCallNotificationClicked() {
        viewState.sendScreeningCallEvent()
    }

    fun onStartRecordingClicked() {
        viewState.sendStartRecordingEvent()
    }

    fun onStopRecordingClicked() {
        viewState.sendStopRecordingEvent()
    }
}