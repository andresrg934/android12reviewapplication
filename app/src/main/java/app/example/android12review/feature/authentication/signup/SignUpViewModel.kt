package app.example.android12review.feature.authentication.signup

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

class SignUpViewModel(
    val presenter: SignUpPresenter,
    val viewState: SignUpViewState
) : ViewModel() {

    internal class Factory(private val componentsProvider: SignUpComponentsProvider) : ViewModelProvider.Factory {
        companion object {
            var newInstance = { componentsProvider: SignUpComponentsProvider ->
                Factory(componentsProvider)
            }
        }

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            val viewState = componentsProvider.getSignUpViewState()
            val presenter = componentsProvider.getSignUpPresenter(viewState)
            return SignUpViewModel(presenter, viewState) as? T ?: throw IllegalArgumentException("This factory can only create MainViewModel instances")
        }
    }
}

internal fun getSignUpViewModel(
    fragmentScope: SignUpFragment
): SignUpViewModel {
    val componentsProvider = SignUpComponentsProvider()
    return ViewModelProviders
        .of(fragmentScope, SignUpViewModel.Factory.newInstance(componentsProvider))
        .get(SignUpViewModel::class.java)
}