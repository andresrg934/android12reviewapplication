package app.example.android12review.feature.authentication.signup

class SignUpComponentsProvider {

    internal fun getSignUpViewState() = SignUpViewState()

    internal fun getSignUpPresenter(viewState: SignUpViewState) = SignUpPresenter(viewState)
}