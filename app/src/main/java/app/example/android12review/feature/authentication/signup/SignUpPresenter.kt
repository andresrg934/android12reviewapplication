package app.example.android12review.feature.authentication.signup

import app.example.android12review.feature.authentication.UserModel

class SignUpPresenter(
    private val viewState: SignUpViewState
) {
    fun onSignUpRequest(user: String, password: String) {
        if (user.isNotEmpty() && password.isNotEmpty()) {
            val userModel = UserModel(user, password)
            viewState.requestSignUp(userModel)
        } else {
            // TODO handle unable to sign up
        }
    }

    fun onSignUpNavigation() {

    }
}