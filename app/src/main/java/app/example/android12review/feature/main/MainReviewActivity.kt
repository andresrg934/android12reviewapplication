package app.example.android12review.feature.main

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import android.content.Context
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import app.example.android12review.databinding.ActivityMainReviewBinding
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main_review.view.*
import android.app.assist.AssistContent
import android.content.pm.PackageManager
import android.net.Uri
import android.media.MediaRecorder
import android.os.Environment
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import app.example.android12review.R
import app.example.android12review.feature.main.CallNotificationsUtil.onShowNotification
import java.io.File

@RequiresApi(Build.VERSION_CODES.S)
class MainReviewActivity : AppCompatActivity() {

    private val onCallEventObserver = Observer<CallEvent> {
        onShowNotification(it)
    }

    private val onRecordingEventObserver = Observer<RecordingEvent> {
        when (it) {
            is RecordingEvent.StartRecording -> startAudioRecording()
            is RecordingEvent.StopRecording -> stopRecording()
        }
    }

    private val mediaRecorder by lazy {
        MediaRecorder().apply {
            val file = File(
                this@MainReviewActivity.getExternalFilesDir(Environment.DIRECTORY_RECORDINGS),
                "recording.mp3"
            )
            file.createNewFile()
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            setOutputFile(file.absolutePath)
        }
    }

    private val viewModel by lazy {
        getMainViewModel(this)
    }

    private val presenter by lazy {
        viewModel.presenter
    }

    private val viewState by lazy {
        viewModel.viewState
    }

    private val binding by lazy {
        ActivityMainReviewBinding.inflate(layoutInflater).apply {
            presenter = this@MainReviewActivity.presenter
        }
    }

    private lateinit var settingsMenuItem: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        installSplashScreen()

        setContentView(binding.root)
        setSupportActionBar(binding.root.toolbar)
        createNotificationChannel()

        viewState.callNotificationEventTrigger.observe(this, onCallEventObserver)
        viewState.recordingEventTrigger.observe(this, onRecordingEventObserver)

        requestPermissions()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        settingsMenuItem = menu.findItem(R.id.menu_item_settings)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onProvideAssistContent(outContent: AssistContent) {
        super.onProvideAssistContent(outContent)
        outContent.webUri = Uri.parse("https://example.com/myCurrentPage")
    }

    private fun createNotificationChannel() {
        val channelID = "NOTIFICATIONS_CHANNEL"
        val channelName = "ING notifications"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelID, channelName, importance)
            // Register the channel with the system
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun requestPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            val permissions = arrayOf(
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            ActivityCompat.requestPermissions(this, permissions, 0)
        }
    }

    private fun startAudioRecording() {
        binding.root.btnStartRecording.isEnabled = false
        binding.root.btnStopRecording.isEnabled = true
        mediaRecorder.prepare()
        mediaRecorder.start()
    }

    private fun stopRecording() {
        binding.root.btnStartRecording.isEnabled = true
        binding.root.btnStopRecording.isEnabled = false
        mediaRecorder.stop()
        mediaRecorder.release()
    }

//    private fun fixWindowsCornerIfNecessary() {
//        // Get the top-right rounded corner from WindowInsets.
//        val insets: WindowInsets = binding.root.rootWindowInsets
//        val topRight = insets.getRoundedCorner(POSITION_TOP_RIGHT) ?: return
//
//        // Get the location of the close button in window coordinates.
//
//        // Get the location of the close button in window coordinates.
//        val location = IntArray(2)
//        settingsMenuItem.getLocationInWindow(location)
//        val buttonRightInWindow: Int = location[0] + closeButton.getWidth()
//        val buttonTopInWindow = location[1]
//
//        // Find the point on the quarter circle with a 45 degree angle.
//
//        // Find the point on the quarter circle with a 45 degree angle.
//        val offset = (topRight.radius * Math.sin(Math.toRadians(45.0))).toInt()
//        val topBoundary = topRight.center.y - offset
//        val rightBoundary = topRight.center.x + offset
//
//        // Check whether the close button exceeds the boundary.
//
//        // Check whether the close button exceeds the boundary.
//        if (buttonRightInWindow < rightBoundary && buttonTopInWindow > topBoundary) {
//            return
//        }
//
//        // Set the margin to avoid truncating.
//
//        // Set the margin to avoid truncating.
//        val parentLocation = IntArray(2)
//        getLocationInWindow(parentLocation)
//        val lp = closeButton.getLayoutParams() as FrameLayout.LayoutParams
//        lp.rightMargin = Math.max(buttonRightInWindow - rightBoundary, 0)
//        lp.topMargin = Math.max(topBoundary - buttonTopInWindow, 0)
//        closeButton.setLayoutParams(lp)
//    }
}