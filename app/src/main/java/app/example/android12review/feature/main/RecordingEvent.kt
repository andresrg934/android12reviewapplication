package app.example.android12review.feature.main

sealed class RecordingEvent {
    object StartRecording: RecordingEvent()
    object StopRecording: RecordingEvent()
}
