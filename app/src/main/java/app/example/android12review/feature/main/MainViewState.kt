package app.example.android12review.feature.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class MainViewState {

    private val _callNotificationEventTrigger = MutableLiveData<CallEvent>()
    val callNotificationEventTrigger: LiveData<CallEvent>
        get() = _callNotificationEventTrigger

    private val _recordingEventTrigger = MutableLiveData<RecordingEvent>()
    val recordingEventTrigger: LiveData<RecordingEvent>
        get() = _recordingEventTrigger

    fun sendIncomingCallEvent() {
        _callNotificationEventTrigger.postValue(CallEvent.IncomingCall)
    }

    fun sendOngoingCallEvent() {
        _callNotificationEventTrigger.postValue(CallEvent.OngoingCall)
    }

    fun sendScreeningCallEvent() {
        _callNotificationEventTrigger.postValue(CallEvent.ScreeningCall)
    }

    fun sendStartRecordingEvent() {
        _recordingEventTrigger.postValue(RecordingEvent.StartRecording)
    }

    fun sendStopRecordingEvent() {
        _recordingEventTrigger.postValue(RecordingEvent.StopRecording)
    }
}