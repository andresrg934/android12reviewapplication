package app.example.android12review.feature.authentication.signup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import app.example.android12review.BR
import app.example.android12review.R
import app.example.android12review.feature.authentication.AuthenticationNavigationEvent
import app.example.android12review.feature.authentication.UserModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlin.coroutines.CoroutineContext

class SignUpFragment(
    onSignUpRequest: suspend (UserModel) -> Boolean,
    onAuthenticationNavigation: (AuthenticationNavigationEvent) -> Unit,
    override val coroutineContext: CoroutineContext = Dispatchers.IO
) : Fragment(), CoroutineScope {

    private val onSignUpObserver = Observer<UserModel> {
        val result = runCatching {
            runBlocking {
                onSignUpRequest(it)
            }
        }.getOrThrow()

        if (!result) {
            // TODO do something in case of failure
        }
    }

    private val viewModel by lazy {
        getSignUpViewModel(this)
    }

    private val presenter by lazy { viewModel.presenter }
    private val viewState by lazy { viewModel.viewState }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.signup_fragment, container, false)
    }

    override fun onViewCreated(safeView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(safeView, savedInstanceState)

        DataBindingUtil.bind<ViewDataBinding>(safeView)?.apply {
            lifecycleOwner = viewLifecycleOwner
        }
        viewState.signupRequestTrigger.observe(viewLifecycleOwner, onSignUpObserver)
    }

}